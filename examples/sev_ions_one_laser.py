# import telegram_send as ts
from sim_functions import *
import matplotlib.pyplot as plt

name = 'sev_ion_one_laser'

#We set some laser parameters
wavelegth = 397e-9
k = [2*np.pi/wavelegth/np.sqrt(3)]*3

#Ion parameters
mass = 40 #Da
charge = 1 #e
cloud_radius = [1e-6,3] #[radius, number of ions] if a float is given then the number of ions is taken as 1 by default

#Trap parameters
trap_radius = 3.75e-3
trap_length = 2.75e-3
trap_kappa  = 0.244
trap_freq = 3.85e6 
voltage = 300
endcapvoltage = 15
anisotropy = 0.9
pseudo = False #not necessary, it is set to False by default
alpha = 0 #not necessary, it is set to 0 by default

#Interaction parameters
gamma = 23e6
detuning = gamma/2

#we create dictionaries with parameters
trap = {'radius': trap_radius, 'length': trap_length, 'kappa': trap_kappa,
        'frequency': trap_freq, 'voltage': voltage, 'endcapvoltage': endcapvoltage,'anisotropy' : anisotropy, 'alpha':alpha, 'pseudo': pseudo}
ions = {'mass': mass, 'charge': charge,'cloud_radius': cloud_radius}

laser = {'gamma': gamma, 
         'detuning': detuning, 
         'k': k} 

# Do simulation
time,positions,velocities = dopplercooling(name,ions,trap,laser,1e7)
# Plot results
fig,ax= plt.subplots(3,sharex =True)
ax[0].plot(time,positions[:,0,0])
ax[1].plot(time,positions[:,0,1])
ax[2].plot(time,positions[:,0,2])
ax[2].set_xlabel('Time (s)')
ax[0].set_ylabel('X (m)')
ax[1].set_ylabel('Y (m)')
ax[2].set_ylabel('Z (m)')
        
#Do pseudopotential simulation
trap.update(pseudo = True)
time,positions,velocities = dopplercooling(name,ions,trap,laser,1e7)

# Plot results
fig_pseudo,ax_pseudo= plt.subplots(3,sharex =True)
ax_pseudo[0].plot(time,positions[:,0,0])
ax_pseudo[1].plot(time,positions[:,0,1])
ax_pseudo[2].plot(time,positions[:,0,2])
ax_pseudo[2].set_xlabel('Time (s)')
ax_pseudo[0].set_ylabel('X (m)')
ax_pseudo[1].set_ylabel('Y (m)')
ax_pseudo[2].set_ylabel('Z (m)')

fig.savefig('doc_figure.png')
fig_pseudo.savefig('doc_figure_pseudo.png')
# ts.send(messages=[f'{name} finished running'])
