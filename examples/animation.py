import numpy as np
import matplotlib.pyplot as plt
import math

from matplotlib import animation, rc

fig, ax = plt.subplots()

frames = 300

results = np.load('sev_ions_one_laser.npz')

positions = results['positions']

line, = ax.plot([],[],'o',ms=10)

ax.set_xlim(np.min(positions[:,:,0]),np.max(positions[:,:,0]))
ax.set_ylim(np.min(positions[:,:,1]),np.max(positions[:,:,1]))

print(positions.shape)
def animate(i):
    line.set_data(positions[i*100,:,0],positions[i*100,:,1])
    return (line,)

anim = animation.FuncAnimation(fig,animate, frames = frames, interval = 50, blit = True)

# plt.show()
anim.save('animation.gif', writer='imagemagick')

