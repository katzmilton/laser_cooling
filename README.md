# Prerequisites

- [LAMMPS](https://www.lammps.org/)

- [(py)Lion](https://bitbucket.org/dtrypogeorgos/pylion/src/master/)

	Follow the documentation of (py)lion to install both packages, you can build LAMMPS from source or directly use the compiled binary file lmp_serial given here placing it on any directory in your PATH.

	There are two corrections to the (py)lion documentation in order to install it correctly: 
	1. setup.py should be run as 
	>python install setup.py
	2. After installation you should copy pylion directory (inside pylion's main directory) to the directory where the rest of your libraries are.

# Modifications of prerequisites

- Use the functions.py given here instead of functions.py given by (py)lion inside pylion directory
- Two options to use LAMMPS modified:

	1. Use the fix_langevin.cpp given here insted of fix_langevin.cpp given by LAMMPS inside src directory (build LAMMPS again once you replace it)
	2. Use the compiled binary file lmp_serial
	
# Usage

To simulate Doppler cooling create three dictionaries, one with ions parameters, one with trap parameters, and one with laser and light-atom interaction parameters.

Then use 

> dopplercooling(name,ions,trap,laser,evolve_steps=False)

to run the simulation.

**arguments:**

**name:** string to give a name to the simulation and the files that will create

**ions:** dictionary with the ion parameters: mass, charge, initial_positions, cloud_radius, N_ions

**trap:** dictionary with the trap parameters: radius, length, kappa, frequency, voltage, endcapvoltage, anisotropy, alpha, pseudo

**laser:** dictionary with the laser and light-atom interacion parameters: gamma, detuning, k, decay_rate, ratio, linear, saturation, emission, T_fin, damp


**returns** three numpy arrays:
- **time:** 1d array with time in seconds
- **positions:** 3d array with position for every step, first index is the step number and it has the same length of the time array, second index is the ion number and third index is the axis: 0 for x, 1 for y and 2 for z.
- **velocities:** 3d array with velocity for every step, first index is the step number and it has the same length of the time array, second index is the ion number and third index is the axis: 0 for x, 1 for y and 2 for z.

# Examples

In the examples directory there are some examples that can be run. Images and data obtained by these examples can be found [here](https://drive.google.com/drive/folders/1eYPNW3jeFz9TL3BxAoSnyR2gROvE6Gy2?usp=sharing)
